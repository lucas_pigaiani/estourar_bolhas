﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_Controller : MonoBehaviour {
	[HideInInspector]
	public Animator enemyAnim; // animator of enemy
	// Use this for initialization
	void Start () {
		enemyAnim = GetComponentInChildren<Animator> (); // getting reference
	}
	
	public void	WinAnim()
	{
		enemyAnim.SetTrigger ("Happy"); // enemy happy anim
	}
	public void LoseAnim()
	{
		enemyAnim.SetTrigger ("Sad"); // enemy sad anim
	}
}
