﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Bubble_Controller : MonoBehaviour {
	
	GameObject FinalPositionBubbles; // reference of position for bubbles destroy
	Spawn_Controller spawn; // reference of spawn
	Button thisButton; // button of bubbles

	int randomInt; // random number for audios

	AudioSource bubaSource; // buba audio source
	AudioSource feedBackSource; // feedback audio source

	Game_Controller gameController; // game controller reference
	Buba_Controller bubaController; // buba reference
	Enemy_Controller enemyController; // enemy reference

	public float tempoFeedBack = 0.1f; // feedbacktime

	bool cancelRepetitiveBl = true;

	// Use this for initialization
	void Start () {
		FinalPositionBubbles = GameObject.FindGameObjectWithTag ("Remove"); // getting reference of final position
		spawn = GameObject.FindGameObjectWithTag ("Spawn_Controller").GetComponent<Spawn_Controller>(); // reference of spawn
		thisButton = this.GetComponent<Button> (); // reference of this button
		thisButton.onClick.AddListener (LettersCheckOnClick); // add a event to on click at this button
		bubaSource = GameObject.FindGameObjectWithTag ("Player").GetComponent<AudioSource>(); // buba audio source reference
		feedBackSource = GameObject.FindGameObjectWithTag ("AudioSource").GetComponent<AudioSource>(); // feedback audio source reference
		bubaController = GameObject.FindGameObjectWithTag ("Player").GetComponentInChildren<Buba_Controller>(); // buba controller reference
		enemyController = GameObject.FindGameObjectWithTag ("Enemy").GetComponentInChildren<Enemy_Controller>(); // enemy controller reference
		gameController = GameObject.FindGameObjectWithTag ("Game_Controller").GetComponent<Game_Controller>(); // game controller reference
	}
	
	// Update is called once per frame
	void Update () {
		if (this.transform.position.y < FinalPositionBubbles.transform.position.y) { // destroy bubble if position < final position
			if (this.GetComponentInChildren<Text>().text == spawn.lettersAudio[gameController.targetLetter].name) // check if the letter is the target letter for the wrong feedback
			{	
				if (cancelRepetitiveBl == true) 
				{
					StartCoroutine (FeedBackWrongTime(tempoFeedBack)); // coroutine of wrong feedback		
					cancelRepetitiveBl = false;
				}

			} 
			else 
			{
				Destroy (this.gameObject); // destroy this bubble
			}		
		}

		if (gameController.gameResetBl == true) { // destroy bubbles with game reset
			Destroy (this.gameObject);
		}


		
	}

	public void	LettersCheckOnClick() // on click event
	{
		if (this.GetComponentInChildren<Text>().text == spawn.lettersAudio[gameController.targetLetter].name) 
		{
			StartCoroutine (FeedBackRightTime(tempoFeedBack)); // if the letter is the target letter call the right feedback
		} 
		else 
		{
			gameController.WrongBubble ();
			StartCoroutine (FeedBackWrongTime(tempoFeedBack)); // if the letter is the wrong lettes, call the wrong feedback

		}
	}

	IEnumerator FeedBackRightTime(float time)
	{
		spawn.thisAudioSource.PlayOneShot (spawn.lettersAudio[gameController.targetLetter]); // audio of right letter
		randomInt = Random.Range (0, 9); // random audio 
		bubaSource.PlayOneShot (bubaController.winSound [randomInt]); // random audio os comemoration
		enemyController.LoseAnim (); // play enemy sad anim
		bubaController.WinAnim (); // play buba happy anim
		feedBackSource.PlayOneShot(bubaController.respostaCerta); // feedback sound
		bubaController.rightFeedBack.enabled = true; // visual efects of right choice
		gameController.RightBubble (); // score plus
		yield return new WaitForSeconds (time);
		bubaController.rightFeedBack.enabled = false; // desactive image of feedback
		if (gameController.score == 5) { // reset the round
			gameController.gameResetBl = true;
			gameController.score = 0;
			gameController.gameTry++;
		}
		Destroy (this.gameObject); // destroy this bubble
	}
	IEnumerator FeedBackWrongTime(float time)
	{
		bubaController.wrongFeedBack.enabled = true; // active wrong feedback image
		randomInt = Random.Range (0, 7); // random audio 
		bubaSource.PlayOneShot (bubaController.loseSound [randomInt]); // sad song play
		feedBackSource.PlayOneShot(bubaController.respostaErrada); // wrong sound feedback
		enemyController.WinAnim(); // enemy happy anim
		bubaController.LoseAnim (); // buba sad anim
		yield return new WaitForSeconds (time); 
		bubaController.wrongFeedBack.enabled = false; // desactive feedback image
		Destroy (this.gameObject); // destroy this bubble

	}
		

}
