﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Buba_Controller : MonoBehaviour {
	[HideInInspector]
	public Animator bubaAnim; // buba animator
	public AudioClip[] loseSound, winSound; // sounds for choices
	public AudioClip respostaCerta, respostaErrada; // sound for right bubble and wrong bubble
	public Image rightFeedBack, wrongFeedBack; // visual effects of right bubble and wrong bubble

	// Use this for initialization
	void Start () {
		bubaAnim = GetComponentInChildren<Animator>(); // getting reference
	}


	public void	WinAnim()
	{
		bubaAnim.SetTrigger ("Happy"); // buba happy anim 
	}
	public void LoseAnim()
	{
		bubaAnim.SetTrigger ("Sad"); // buba sad anim
	}

}
