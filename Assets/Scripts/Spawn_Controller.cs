﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Spawn_Controller : MonoBehaviour {

	public AudioClip[] lettersAudio; // sound of letters
	public GameObject lettersAnchor; // reference for spawn letters
	public Rigidbody2D lettersObj; // prefab of letters


	[HideInInspector]
	public AudioSource thisAudioSource; // audio player

	[HideInInspector]
	public bool spawnBl; // boolean of spawn right or wrong letters
	[HideInInspector]
	public bool canSpawnBl; // spawn letters controler
	int spawnInt; // counter of spawn
	int spawnTarget; // spawn controller 
	Game_Controller gameController; // game controller reference

	// Use this for initialization
	void Start () {
		thisAudioSource = this.GetComponent<AudioSource> (); // getting reference of this audio source
		gameController = GameObject.FindGameObjectWithTag ("Game_Controller").GetComponent<Game_Controller>(); //getting game controller reference
	}
	
	// Update is called once per frame
	void Update () {

		if (canSpawnBl == true) { // check of spawn
			if (spawnBl == true) // spawn controller
			{
				if (spawnTarget < 4 * gameController.score + 2) { // spawn wrong letter
					Invoke ("SpawnLetters", 1);
					spawnBl = false;
				}
				if (spawnTarget >= 4 * gameController.score + 2) { // spawn right letter
					Invoke ("SpawnTargetLetters", 1);
					spawnBl = false;
				}
			}
		}


	}
	public void SpawnLetters() //spawn letter with a chance between 26 to be the rght letter
	{
		CancelInvoke ("SpawnLetters"); // cancel repetitive invokes
		spawnInt = Random.Range (0, 26); // random letter 
		Rigidbody2D clone; // letters spawn object
		clone = Instantiate(lettersObj, lettersAnchor.transform.position, transform.rotation) as Rigidbody2D; // instantiate letters
		clone.velocity = lettersAnchor.transform.TransformDirection(transform.up*100);
		clone.transform.parent = lettersAnchor.transform;
		clone.GetComponentInChildren<Text> ().text = lettersAudio [spawnInt].name; // set letter to object
		spawnTarget++; 
		spawnBl = true;


	}
	public void SpawnTargetLetters() // spawn the right letter
	{
		CancelInvoke ("SpawnLetters"); // cancel repetitive invokes
		Rigidbody2D clone; // letters spawn object
		clone = Instantiate(lettersObj, lettersAnchor.transform.position, transform.rotation) as Rigidbody2D; // instantiate letters
		clone.velocity = lettersAnchor.transform.TransformDirection(transform.up*100);
		clone.transform.parent = lettersAnchor.transform;
		clone.GetComponentInChildren<Text> ().text = lettersAudio [gameController.targetLetter].name; // set letter to object
		spawnTarget = 0;
		spawnBl = true;

	}

	
}
