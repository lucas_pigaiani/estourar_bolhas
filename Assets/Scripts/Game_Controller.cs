﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Game_Controller : MonoBehaviour {

	public Image stars; // stars image, whow to player the score of the round
	public Text scoreText; // show the score of the round
	Spawn_Controller spawnController; // spawn controller reference

	public Text instructionText, timerText; // show to player the instructions and the timer of scene

	[HideInInspector]
	public int targetLetter; // target letter 

	float gameTimeInitial = 130f; // time reference
	float gameTime = 130f; // scene time 
	float initialtime = 6; // countdown time reference
	float time = 6; // countdown time
	bool timeCountdown = true; // countdown bool

	bool gameStartBl = true; // reference bool for play game
	public bool gameResetBl = false; // reference bool for reset game

	public int score, finalScore, errors; // points of round, total points and errors


	public GameObject panelFinalScore; // panel final score reference
	public Image finalScoreImg; // final score image
	public Text finalScoreText; // final score text
	public GameObject resetButton, quitButton; // close and reset button

	public int gameTry = 0; // game round

	// Use this for initialization
	void Start () {			
		spawnController = GameObject.FindGameObjectWithTag ("Spawn_Controller").GetComponent<Spawn_Controller>(); // getting reference
	}
	
	// Update is called once per frame
	void Update () {
		if (gameStartBl == true) { // game initialize
			GameStart ();
		}
		if (gameResetBl == true) { // game reset
			SceneReset ();
		}
		if (timeCountdown == false) { // game play
			gameTime -= Time.deltaTime; 
			timerText.text =  Mathf.RoundToInt(gameTime).ToString ();
		}
		if (gameTry == 3 || gameTime <= 0) { // game end
			EndGame ();
		}
	}

	void GameStart()
	{
		// game countdown ---------------------
		if (time > 0 && timeCountdown == true) 
		{
			time -= Time.deltaTime;
			if (time >= 5 && time <= 6) {
				instructionText.text = "5";
			}
			if (time >= 4 && time <= 5) {
				instructionText.text = "4";
			}
			if (time >= 3 && time <= 4) {
				instructionText.text = "3";
			}
			if (time >= 2 && time <= 3) {
				instructionText.text = "2";
			}
			if (time >= 1 && time <= 2) {
				instructionText.text = "1";
			}
			if (time >= 0 && time <= 1) {
				instructionText.text = "0";
			}
		} 
		// -------------------------------------------
		if (time <= 0 && timeCountdown == true) 		// game start
		{
			targetLetter = Random.Range(0,26);
			instructionText.text = "Clique na bolha que contém a letra: " + spawnController.lettersAudio[targetLetter].name;
			spawnController.thisAudioSource.PlayOneShot (spawnController.lettersAudio[targetLetter]);	
			spawnController.spawnBl = true;
			timeCountdown = false;
			gameStartBl = false;
			spawnController.canSpawnBl = true;
			timerText.text = Mathf.RoundToInt(gameTime).ToString ();
		}
	}

	void SceneReset() // variables reset
	{
		time = initialtime;
		timeCountdown = true;
		gameStartBl = true;
		spawnController.canSpawnBl = false;
		gameResetBl = false;
	}

	public void RightBubble() // score plus
	{

		score++;
		finalScore++;
		scoreText.text = score.ToString ();
		stars.fillAmount += 0.067f;
	}
	public void WrongBubble() // errors plus
	{
		errors++;
	}

	public void EndGame() // end game method
	{
		panelFinalScore.SetActive (true);
		finalScoreText.text = "Você fez uma pontuação média de " + (finalScore - errors) / 3;

		if ((finalScore - errors) / 3 == 5) {
			finalScoreImg.fillAmount = 1f;
		}
		if ((finalScore - errors) / 3 == 4) {
			finalScoreImg.fillAmount = 0.8f;
		}
		if ((finalScore - errors) / 3 == 3) {
			finalScoreImg.fillAmount = 0.6f;
		}
		if ((finalScore - errors) / 3 == 2) {
			finalScoreImg.fillAmount = 0.4f;
		}
		if ((finalScore - errors) / 3 == 1) {
			finalScoreImg.fillAmount = 0.2f;
		}
		if ((finalScore - errors) / 3 == 0) {
			finalScoreImg.fillAmount = 0f;
		}

		//finalScoreImg.fillAmount =  (finalScore - errors) / 15;
		Time.timeScale = 0;
		resetButton.SetActive (true);
		quitButton.SetActive (true);
	}

	public void ResetGame() // reset game
	{
		Time.timeScale = 1;
		gameTry = 0;
		gameTime = initialtime;
		Application.LoadLevel (0);	
	}
	public void QuitGame() // close game
	{
		Application.Quit ();
	}

}
